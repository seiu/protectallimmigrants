<?php
/*
* Category Template: Press Release (Spanish)
*  
*/
?>



<?php
get_header(); 
?>
 
<h2>Comunicados de Prensa</h2>

<div class="flexcontainer">
<?php


if ( have_posts() ) {

	// Start the Loop.
	while ( have_posts() ) :
		the_post();
		get_template_part( 'partials/content-press', 'excerpt' );
	endwhile;

	// Previous/next page navigation.
	get_template_part( 'partials/pagination' );

} else {

	// If no content, include the "No posts found" template.
	get_template_part( 'partials/content', 'none' );
}

?>

</div>

<?php
get_footer();
?>