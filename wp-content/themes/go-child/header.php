<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="site-content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Go
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>

<link rel="shortcut icon" href="https://protectallimmigrants.org/wp-content/uploads/2020/04/ProtectALLImmigrantsSquare.jpg">

	<style type="text/css">
		body.category-12 h2,
		body.category-16 h2,
		body.category-14 h2,
		body.category-18 h2 { 
			text-align: center; 
			}
		.flexcontainer {
			display: flex;
  			flex-wrap: wrap;
  			max-width: 90vw;
  			margin: 0 auto;
  			justify-content: space-evenly;
			}
		.flexcontainer article {
			width: 45%;
			border-radius: 10px;
			border: 1px solid #c7cac8;
			margin-bottom: 2rem;
			padding: 1rem;
			}

		body.category-16 .flexcontainer article,
		body.category-18 .flexcontainer article {
			width: 100%;
			}

		.flexcontainer article h4 {
			margin-bottom: .5rem;
			font-size: 1.7rem;
			}
		
		body.category-16 .flexcontainer article h4,
		body.category-18 .flexcontainer article h4 {
			font-size: 2rem;
			}

		.flexcontainer article h4 a:link,
		.flexcontainer article h4 a:visited,
		.flexcontainer article h4 a:active,
		.flexcontainer article h4 a:focus {
			text-decoration: none;
			font-weight: bold;
			color: #1046ba;
			}

		.flexcontainer article h4 a:hover {
			text-decoration: underline;
			color: #0c4392;
			}
			
		.cont-area-wrap ul.post__meta li.post-author,
		.cont-area-wrap ul.post__meta li.post-comment-link { 
			display: none; 
			}

		.cont-area-wrap ul.post__meta {
			display: inline;
			}

		.cont-area-wrap ul.post__meta li.post-date {
			margin: 0;
			padding: 0;
			display: inline-block;
			font-size: 1rem;
			}

		@media only screen and (max-width: 700px) {
			.flexcontainer article {
				width: 100%;
			}
		}

	</style>
</head>

<body
	<?php
	$body_class = get_body_class();
	if ( Go\AMP\is_amp() ) {
		?>
		aria-expanded="false"
		[aria-expanded]="mainNavMenuExpanded ? 'true' : 'false'"
		[class]="'<?php echo esc_attr( implode( ' ', $body_class ) ); ?>' + ( mainNavMenuExpanded ? ' menu-is-open' : '' )"
		<?php
	}
	?>
	class="<?php echo esc_attr( implode( ' ', $body_class ) ); ?>"
>

	<?php wp_body_open(); ?>

	<div id="page" class="site">

		<a class="skip-link screen-reader-text" href="#site-content"><?php esc_html_e( 'Skip to content', 'go' ); ?></a>

		<header id="site-header" class="site-header header relative <?php echo esc_attr( Go\has_header_background() ); ?>" role="banner" itemscope itemtype="http://schema.org/WPHeader">

			<div class="header__inner flex items-center justify-between h-inherit w-full relative">

				<div class="header__extras">
					<?php Go\search_toggle(); ?>
					<?php Go\WooCommerce\woocommerce_cart_link(); ?>
				</div>

				<div class="header__title-nav flex items-center flex-nowrap">

					<?php Go\display_site_branding(); ?>

					<?php if ( has_nav_menu( 'primary' ) ) : ?>

						<nav id="header__navigation" class="header__navigation" aria-label="<?php esc_attr_e( 'Horizontal', 'go' ); ?>" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">

							<div class="header__navigation-inner">
								<?php
								wp_nav_menu(
									array(
										'menu_class'     => 'primary-menu list-reset',
										'theme_location' => 'primary',
									)
								);
								?>
							</div>

						</nav>

					<?php endif; ?>

				</div>

				<?php Go\navigation_toggle(); ?>

			</div>

			<?php get_template_part( 'partials/modal-search' ); ?>

		</header>

		<main id="site-content" class="site-content" role="main">
