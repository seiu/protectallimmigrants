<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Go
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">


	<?php  the_title( sprintf( '<h4><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' ); ?>



	<div class="cont-area-wrap">
		<?php the_excerpt(); ?>


		<?php Go\post_meta( get_the_ID(), 'top' ); ?>
	</div>

	





</article>
