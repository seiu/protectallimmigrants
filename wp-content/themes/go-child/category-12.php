<?php
/*
* Category Template: News (English)
*/
?>



<?php
get_header(); 
?>
 
<h2>News</h2>
<div class="flexcontainer">
	<p><a href="https://protectallimmigrants.org/es/category/noticias/">Español</a></p>	
</div>
<div class="flexcontainer">


<?php


if ( have_posts() ) {

	// Start the Loop.
	while ( have_posts() ) :
		the_post();
		get_template_part( 'partials/content-news', 'excerpt' );
	endwhile;

	// Previous/next page navigation.
	get_template_part( 'partials/pagination' );

} else {

	// If no content, include the "No posts found" template.
	get_template_part( 'partials/content', 'none' );
}

?>

</div>

<?php
get_footer();
?>